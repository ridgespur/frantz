<?php
/**
 * Template Name: Homepage
 */
?>

<div class="pad-top-2 pad-bot row">
	<?php get_template_part('templates/cards'); ?> 
</div>

<?php while (have_posts()) : the_post(); ?>
  <?php // get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>