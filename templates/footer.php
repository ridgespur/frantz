<footer class="content-info">
  <div class="container-fluid pad-top">
  	<div class="row">
  		<?php get_template_part('templates/socials'); ?> 
    </div>
  </div>
  
  <div class="container pad-top">
	<div class="col-lg-4 text-left col-sm-6">
		<p>The Conservatory of Music <?php echo date("Y"); ?></p>
	</div>
	<div class="col-lg-4 text-center">
		<p>1601 Lawrenceville Road • Lawrenceville, NJ 08550</p>
	</div>
	<div class="col-lg-4 text-right">
		<?php dynamic_sidebar('sidebar-footer'); ?>
	</div>
	
  </div>
</footer>
