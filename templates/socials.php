<div class="row socials">
	<div class="col-lg-4 nopadding">
		<a class="btn btn-success btn-block pad-top pad-bot text-right" href="http://www.google.com/url?q=http%3A%2F%2Fwww.facebook.com%2FConservatory-of-Music-1117730648237932&sa=D&sntz=1&usg=AFQjCNER7gFqyFYM4B456WXYvcRANxznxw"><i class="fa fa-lg fa-facebook"> Like Us On Facebook</i></a>
	</div>

	<div class="col-lg-4 nopadding">
		<a class="btn btn-primary btn-block pad-top pad-bot text-center" href="https://twitter.com/drfrantzmusic"><i class="fa fa-lg fa-twitter"> Follow Us On Twitter</i></a>
	</div>

	<div class="col-lg-4 nopadding">
		<a class="btn btn-warning btn-block pad-top pad-bot text-left" href="plus.google.com/18005198488769854549"><i class="fa fa-lg fa-google-plus"> Follow Us On Google+</i></a>
	</div>
</div>