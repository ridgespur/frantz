<header class="banner">
  <div class="container">
    <div>&nbsp;</div>
    <!-- <a class="brand" href="<?= esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?></a> -->
    <nav class="navbar" role="navigation">
      <div class="container-fluid">
        <div class="row">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header col-xs-9 col-sm-5 nopadding-mobile">
              <a class="navbar-brand text-center nopadding-mobile" href="<?php echo home_url(); ?>">
                <span class="bolder twenty-six color-secondary"><?php bloginfo('name'); ?></span>
                <div class="clearfix"></div>
                <span class="text-muted medium color-primary"><?php bloginfo('description' ); ?></span>
              </a>
            </div>
            
            <button type="button" class="navbar-toggle bg-primary" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar bg-white"></span>
                <span class="icon-bar bg-white"></span>
                <span class="icon-bar bg-white"></span>
            </button>

                <?php
                    wp_nav_menu( array(
                        'menu'              => 'primary_navigation',
                        'theme_location'    => 'primary_navigation',
                        'depth'             => 4,
                        'container'         => 'div',
                        'container_class'   => 'collapse navbar-collapse pull-right',
                        'container_id'      => 'bs-example-navbar-collapse-1',
                        'menu_class'        => 'nav navbar-nav',
                        'menu_id'           => 'menu-soil-nav',
                        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                        'walker'            => new wp_bootstrap_navwalker())
                    );
                ?>
            </div>
        </div>
    </nav>

  </div>
</header>