<?php

$args = array( 'post_type' => 'card', 'posts_per_page' => 20);
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post();
$youtube = get_field("youtube");

?>
    
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">

<?php

    if ( has_post_thumbnail() ) {
        $thumb_id = get_post_thumbnail_id();
        $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'medium');
        $thumb_url = $thumb_url_array[0];
?>
		<img class="img-card" style="background-image: url('<?= $thumb_url; ?>');">
<?php
    	
	} elseif ($youtube != null) {
?>
	<div class="embed-responsive embed-responsive-4by3">
 		 <iframe class="embed-responsive-item" src="<?=$youtube  ?>"></iframe>
	</div>


<?php


	}

?>

      <div class="caption">
        <h3><?= the_title(); ?></h3>
        <p><?= the_excerpt(); ?></p>
        <p><a href="<?php the_field('button_link'); ?>" class="btn btn-primary" role="button"><?php the_field('button_text'); ?></a>
      </div>
    </div>
  </div>



<!-- hello -->
<?php endwhile; ?>

